﻿CREATE TABLE [dbo].[UserRole]
(
	[UserRoleId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[UserId] INT NOT NULL CONSTRAINT [FK_UserRole_User1] REFERENCES [dbo].[User] (UserId),
	[RoleId] INT NOT NULL CONSTRAINT [FK_UserRole_Role] REFERENCES [dbo].[Role] (RoleId),
	[Active] BIT NOT NULL DEFAULT 0,
	[CreateDate] DATETIME NOT NULL DEFAULT GETDATE(),
	[CreatedByUserId] INT NOT NULL CONSTRAINT [FK_UserRole_User2] REFERENCES [dbo].[User] (UserId)
)
