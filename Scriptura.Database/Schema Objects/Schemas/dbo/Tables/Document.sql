﻿CREATE TABLE [dbo].[Document]
(
	[DocumentId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[CategoryId] INT NOT NULL CONSTRAINT [FK_Document_Category] REFERENCES [dbo].[Category] (CategoryId),
	[Filename] VARCHAR(255) NOT NULL,
	[Approved] BIT NOT NULL DEFAULT (0),
	[CreatedByUserId] INT NOT NULL CONSTRAINT [FK_Document_User] REFERENCES [dbo].[User] (UserId),
	[CreateDate] DATETIME NOT NULL DEFAULT GETDATE(),
	[ApprovedByUserId] INT NOT NULL CONSTRAINT [FK_Document_User2] REFERENCES [dbo].[User] (UserId),
	[ApproveDate] DATETIME NULL,
	[DeclineReason] VARCHAR(500) NULL
)
