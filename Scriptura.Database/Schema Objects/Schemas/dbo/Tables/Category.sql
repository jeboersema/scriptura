﻿CREATE TABLE [dbo].[Category]
(
	[CategoryId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[CategoryName] VARCHAR(100) NOT NULL
)
