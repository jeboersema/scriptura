﻿CREATE TABLE [dbo].[Priviledge]
(
	[PriviledgeId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Description] VARCHAR(255) NOT NULL,
	[Active] BIT NOT NULL DEFAULT (0),
	[CreateDate] DATETIME NOT NULL DEFAULT GETDATE()
)
