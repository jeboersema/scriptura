﻿CREATE TABLE [dbo].[RolePriviledge]
(
	[RolePriviledgeId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[RoleId] INT NOT NULL CONSTRAINT [FK_RolePriviledge_Role] REFERENCES [dbo].[Role] (RoleId),
	[PriviledgeId] INT NOT NULL CONSTRAINT [FK_RolePriviledge_Priviledge] REFERENCES [dbo].[Priviledge] (PriviledgeId),
	[CreateDate] DATETIME NOT NULL DEFAULT GETDATE()
)
