﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

if not exists(select null from [dbo].[User] where UserId = 0)
begin
	set identity_insert [dbo].[User] on

	insert into [dbo].[User] (UserId, Username, [Password], [FullName], [EmailAddress], [MobileNo], [Active], [ApprovedByUserId])
	values (0, 'System', '', 'Internal System User', 'jeboersema@gmail.com', '0827853189', 1, 0)

	set identity_insert [dbo].[User] off
end