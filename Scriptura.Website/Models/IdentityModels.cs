﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Scriptura.Website.Models
{
	public class ApplicationUser : IdentityUser
	{
	}

	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext()
			: base("DefaultConnection")
		{
		}
	}
}