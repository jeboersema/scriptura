﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Web.Mvc;

namespace Scriptura.Website.Models
{
	public class ExternalLoginConfirmationViewModel
	{
		[Required]
		[Display(Name = "User name")]
		public string UserName { get; set; }
	}

	public class ManageUserViewModel
	{
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Bestaande wagwoord")]
		public string OldPassword { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "Die {0} moet ten minste {2} karakters lank wees.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Nuwe wagwoord")]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Bevestig nuwe wagwoord")]
		[System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Die nuwe wagwoord en die bevestiging van die nuwe wagwoord is nie dieselfde nie.")]
		public string ConfirmPassword { get; set; }
	}

	public class LoginViewModel
	{
		[Required]
		[Display(Name = "E-pos Adres")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Wagwoord")]
		public string Password { get; set; }

		[Display(Name = "Onthou my?")]
		public bool RememberMe { get; set; }
	}

	public class RegisterViewModel
	{
    [Required]
    [Display(Name = "Naam en Van")]
    public string FullName { get; set; }
    
    [Required]
		[Display(Name = "E-pos Adres")]
    [EmailAddress(ErrorMessage = "Die e-pos adres is nie 'n geldige e-pos adres nie.")]
    [Remote("IsEmailAddressAvailable", "Account", ErrorMessage = "Die e-pos adres is reeds geregistreer.")]
		public string Email { get; set; }

    [Required]
		[StringLength(100, ErrorMessage = "Die {0} moet ten minste {2} karakters lank wees.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Wagwoord")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Bevestig wagwoord")]
		[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Die wagwoord en die bevestiging van die wagwoord is nie dieselfde nie.")]
		public string ConfirmPassword { get; set; }
	}

}