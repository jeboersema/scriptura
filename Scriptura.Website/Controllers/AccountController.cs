﻿using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Scriptura.Website.Models;
using Scriptura.Website.Data;

namespace Scriptura.Website.Controllers
{
  [Authorize]
  public class AccountController : Controller
  {
    public AccountController()
      : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
    {
    }

    public AccountController(UserManager<ApplicationUser> userManager)
    {
      UserManager = userManager;
    }

    public UserManager<ApplicationUser> UserManager { get; private set; }

    //
    // GET: /Account/Login
    [AllowAnonymous]
    public ActionResult Login(string returnUrl)
    {
      ViewBag.ReturnUrl = returnUrl;
      return View();
    }

    // POST: /Account/Login
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult Login([Bind(Include = "UserName,Password,RememberMe")] LoginViewModel login)
    {
      if (ModelState.IsValid)
      {
        var r = UserDataAccess.Authenticate(login.UserName, login.Password);
        if (r == 0)
        {
          return RedirectToAction("Index", "Home");
        }
      }

      return View(login);
    }
    
    //
    // GET: /Account/Register
    [AllowAnonymous]
    public ActionResult Register()
    {
      return View(new RegisterViewModel());
    }

    // POST: /Account/Register
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult Register([Bind(Include="FullName,Email,Password,ConfirmPassword")] RegisterViewModel register)
    {
      if (ModelState.IsValid)
      {
        UserDataAccess.CreateUserAccount(register);

        return RedirectToAction("Login");
      }

      return View(register);
    }

    [AllowAnonymous]
    public JsonResult IsEmailAddressAvailable(string email)
    {
      var exists = UserDataAccess.HasEmailAddressBeenUsed(email);

      if (exists)
        return Json("Die e-pos adres is reeds gebruik", JsonRequestBehavior.AllowGet);
      return Json(true, JsonRequestBehavior.AllowGet);
    }
  }
}