//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scriptura.Website.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role
    {
        public Role()
        {
            this.RolePriviledge = new HashSet<RolePriviledge>();
            this.UserRole = new HashSet<UserRole>();
        }
    
        public int RoleId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public System.DateTime CreateDate { get; set; }
    
        public virtual ICollection<RolePriviledge> RolePriviledge { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
