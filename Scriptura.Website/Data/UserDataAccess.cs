﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Scriptura.Website.Models;

namespace Scriptura.Website.Data
{
  public class UserDataAccess : DataAccessBase
  {
    public static void CreateUserAccount(RegisterViewModel model)
    {
      var db = ConnectToDatabase();

      var user = new User
      {
        Active = false,
        CreateDate = DateTime.Now,
        EmailAddress = model.Email,
        FullName = model.FullName,
        Username = model.Email,
        Password = model.Password
      };

      db.User.Add(user);
      try
      {
        db.SaveChanges();
      }
      catch (DbEntityValidationException ex)
      {
        
      }
    }

    public static bool HasEmailAddressBeenUsed(string email)
    {
      var db = ConnectToDatabase();
      email = email.ToLower();

      var user = db.User.FirstOrDefault(u => u.EmailAddress.ToLower() == email);

      return (user != null);
    }

    public static int Authenticate(string email, string password)
    {
      var db = ConnectToDatabase();
      email = email.ToLower();

      var user = db.User.FirstOrDefault(u => u.EmailAddress.ToLower() == email);
      if (user == null) return 1;
      if (user.Password != password) return 1;
      if (!user.Active) return 2;
      return 0;
    }
  }
}